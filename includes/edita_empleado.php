<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
    include_once 'conexion.php';
    $conexion = new Conexion();

    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (!isset($_POST['primer_nombre'])||!isset($_POST['primer_apellido'])||!isset($_POST['genero'])||!isset($_POST['celular'])||empty($_POST['primer_nombre'])||empty($_POST['primer_apellido'])||empty($_POST['genero'])||empty($_POST['celular'])) {
            $error = "Los campos, <b>Primer Nombre, Primer Apellido, Genero y Nro. Celular</b> son obligatorios";
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));
            exit();
        }
        $primer_nombre = htmlentities(strip_tags(trim($_POST['primer_nombre'])));
        $segundo_nombre = htmlentities(strip_tags(trim($_POST['segundo_nombre'])));
        $primer_apellido = htmlentities(strip_tags(trim($_POST['primer_apellido'])));
        $segundo_apellido = htmlentities(strip_tags(trim($_POST['segundo_apellido'])));
        $genero = htmlentities(strip_tags(trim($_POST['genero'])));
        $celular = htmlentities(strip_tags(trim($_POST['celular'])));
        $direccion = htmlentities(strip_tags(trim($_POST['direccion'])));
        $fotografia = htmlentities(strip_tags(trim($_POST['antigua_fotografia'])));
        $id = base64_decode(base64_decode(base64_decode(base64_decode(($_POST['id_empleado'])))));

        if (isset($_FILES['fotografia'])) {
            if (!empty($_FILES['fotografia']['name'])) {
                $tipo = $_FILES['fotografia']['type'];
                if ($tipo=="image/png"||$tipo=="image/jpeg") {
                    $nombre_fotografia = $_FILES['fotografia']['name'];
                    $extension = pathinfo($nombre_fotografia, PATHINFO_EXTENSION);
                    $ruta_temporal = $_FILES['fotografia']['tmp_name'];
                    $nuevo_nombre =  "empleado_". bin2hex(random_bytes(8)) . date('Ymd_His') . '.'.$extension;
                    $ruta_destino = "../fotografia-empleado/normal/$nuevo_nombre";
                    if (move_uploaded_file($ruta_temporal, $ruta_destino)){
                        $fotografia = $nuevo_nombre;
                    }else{
                        $error = "No se pudo subir la fotografia, intente nuevamente por favor";
                    }
                }else{
                    $error = "El tipo de archivo no es permitido, por favor seleccione una imagen JPG o PNG";
                }
            }
        }
        if (isset($error)) {
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));
            exit();
        }
        $query = $conexion->connect()->prepare('UPDATE empleados SET primer_nombre = :primer_nombre, segundo_nombre = :segundo_nombre, primer_apellido = :primer_apellido, segundo_apellido = :segundo_apellido, genero = :genero, celular = :celular, direccion = :direccion, fotografia = :fotografia WHERE id = :id');
        $query->execute(
            array(
                ':primer_nombre' => $primer_nombre,
                ':segundo_nombre' => $segundo_nombre,
                ':primer_apellido' => $primer_apellido,
                ':segundo_apellido' => $segundo_apellido,
                ':genero' => $genero,
                ':celular' => $celular,
                ':direccion' => $direccion,
                ':fotografia' => $fotografia,
                ':id' => $id,
            )
        );

        $mensaje = "Datos actualizados correctamente";
        setcookie("confirmado", $mensaje, time() + 10, '/');

        header('location: ../vistas/editar_empleado.php?empleado='.base64_encode(base64_encode(base64_encode(base64_encode($id)))));
    }else{
        header('location: ../index.php');
    }
}else{
  header('location: ../index.php');
}

?>
?>