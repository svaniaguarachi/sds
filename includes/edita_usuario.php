<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
    include_once 'conexion.php';
    $conexion = new Conexion();

    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (!isset($_POST['primer_nombre'])||!isset($_POST['primer_apellido'])||!isset($_POST['rol'])||!isset($_POST['usuario'])||empty($_POST['primer_nombre'])||empty($_POST['primer_apellido'])||empty($_POST['rol'])||empty($_POST['usuario'])) {
            $error = "Los campos, <b>Primer Nombre, Primer Apellido, Rol y Usuario</b> son obligatorios";
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));
            exit();
        }
        $primer_nombre = htmlentities(strip_tags(trim($_POST['primer_nombre'])));
        $segundo_nombre = htmlentities(strip_tags(trim($_POST['segundo_nombre'])));
        $primer_apellido = htmlentities(strip_tags(trim($_POST['primer_apellido'])));
        $segundo_apellido = htmlentities(strip_tags(trim($_POST['segundo_apellido'])));
        $rol = htmlentities(strip_tags(trim($_POST['rol'])));
        $usuario = htmlentities(strip_tags(trim($_POST['usuario'])));
        $id = base64_decode(base64_decode(base64_decode(base64_decode(($_POST['id_usuario'])))));

        $query = $conexion->connect()->prepare('UPDATE usuarios SET primer_nombre = :primer_nombre, segundo_nombre = :segundo_nombre, primer_apellido = :primer_apellido, segundo_apellido = :segundo_apellido, rol = :rol, usuario = :usuario WHERE id = :id');
        $query->execute(
            array(
                ':primer_nombre' => $primer_nombre,
                ':segundo_nombre' => $segundo_nombre,
                ':primer_apellido' => $primer_apellido,
                ':segundo_apellido' => $segundo_apellido,
                ':rol' => $rol,
                ':usuario' => $usuario,
                ':id' => $id,
            )
        );

        $mensaje = "Datos actualizados correctamente";
        setcookie("confirmado", $mensaje, time() + 10, '/');

        header('location: ../vistas/editar_usuario.php?usuario='.base64_encode(base64_encode(base64_encode(base64_encode($id)))));
    }else{
        header('location: ../index.php');
    }
}else{
  header('location: ../index.php');
}

?>
?>