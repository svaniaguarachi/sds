<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
	include_once 'conexion.php';
	$conexion = new Conexion();

	if (!isset($_GET['empleado'])||empty($_GET['empleado'])) {
		header('location: ../vistas/listado_empleados.php');
		exit();
	}
	$id = base64_decode(base64_decode(base64_decode(base64_decode($_GET['empleado']))));

	$query = $conexion->connect()->prepare('DELETE FROM empleados WHERE id = :id');
	$query->execute(array(':id' => $id));

	$mensaje = "Se eliminó al empleado correctamente";
	setcookie("confirmado", $mensaje, time() + 10, '/');
	header('location: ../vistas/listado_empleados.php');

}else{
	header('location: ../index.php');
}

?>